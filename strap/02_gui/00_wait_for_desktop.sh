#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Wait for the desktop to boot and set the display for running the script.'
sleep 1m