#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Configure keyboard shortcuts.'

KP=/com/canonical/unity/settings-daemon/plugins/media-keys/custom-keybindings

function custom_binding() {
    ID=$1
    NAME=$2
    BINDING=$3
    COMMAND=$4
    local GPATH=$KP/custom${ID}

    dconf write "$GPATH/name" "$NAME"
    dconf write "$GPATH/binding" "$BINDING"
    dconf write "$GPATH/command" "$COMMAND"
}

function join() {
    local IFS="$1"
    shift
    echo "$*"
}

function custom_link() {
    N=$1
    local arr=()
    for i in $(seq 0 $N)
    do
        arr[${#arr[@]}]="'$KP/custom$i/'"
    done
    join , ${arr[@]}
}

custom_binding 0 "'Nemo'" "'<Primary><Alt>H'" "'nemo'"
custom_binding 1 "'GEdit'" "'<Primary><Alt>G'" "'gedit'"
custom_binding 2 "'CoppeliaMedia All Video'" "'<Primary><Alt>1'" "'$HOME/Software/control-video all-video'"
custom_binding 3 "'CoppeliaMedia All Audio'" "'<Primary><Alt>2'" "'$HOME/Software/control-video all-audio'"
custom_binding 4 "'CoppeliaMedia Item Next'" "'<Primary><Alt>N'" "'$HOME/Software/control-video item next'"
custom_binding 5 "'CoppeliaMedia Item Back'" "'<Primary><Alt><Shift>N'" "'$HOME/Software/control-video item back'"
custom_binding 6 "'CoppeliaMedia Playback Toggle'" "'<Primary><Alt>P'" "'$HOME/Software/control-video playback toggle'"
custom_binding 7 "'Calculator'" "'<Primary><Alt>C'" "'gnome-calculator'"
custom_binding 8 "'Screenshot'" "'<Print>'" "'$HOME/Software/screenshot'"
custom_binding 9 "'Screenshot'" "'<Primary><Alt>F'" "'google-chrome'"

N=9

ARR=$(custom_link $N)
dconf write $KP "[${ARR}]"
