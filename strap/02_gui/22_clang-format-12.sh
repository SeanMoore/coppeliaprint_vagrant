#!/bin/bash
set -xeuo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Install clang-format-12.'
case "$(lsb_release -rs)" in
20.04)
    sudo apt-get install -y clang-format-12
    sudo update-alternatives --install \
        /usr/bin/clang-format \
        clang-format \
        /usr/bin/clang-format-12 \
        20 \
    ;
    ;;
*)
    echo "Unhandled system."
    exit
    ;;
esac