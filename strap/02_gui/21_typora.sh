#!/bin/bash
set -xeuo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Install Typora.'
sudo snap install typora --classic