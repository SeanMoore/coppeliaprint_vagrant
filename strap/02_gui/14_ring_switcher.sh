#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Set up Ring Switcher.'
sudo apt-get install -y compiz-plugins-extra