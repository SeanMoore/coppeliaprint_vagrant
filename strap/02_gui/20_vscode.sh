#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Install Visual Studio Code.'
pushd /tmp >& /dev/null
    wget https://update.code.visualstudio.com/latest/linux-deb-x64/stable -O vscode.deb
    sudo dpkg -i vscode.deb # For reference the package name is "code".
popd >& /dev/null

VSCODE_CONFIG=$HOME/.config/Code/User
mkdir -p $VSCODE_CONFIG

cat > $VSCODE_CONFIG/settings.json << EOF
{
    "editor.formatOnSave": true,
    "editor.inlayHints.enabled": "offUnlessPressed",
}
EOF

cat > $VSCODE_CONFIG/keybindings.json << EOF
[
    {
        "key": "ctrl+alt+[",
        "command": "editor.foldAll",
        "when": "editorTextFocus"
    },
    {
        "key": "ctrl+alt+]",
        "command": "editor.unfoldAll",
        "when": "editorTextFocus"
    },
]
EOF

code \
    --install-extension ms-vscode.cpptools \
    --install-extension bungcip.better-toml \
    --install-extension rust-lang.rust \
;