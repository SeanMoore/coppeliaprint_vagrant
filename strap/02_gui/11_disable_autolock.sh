#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Turn off automatic lock screen.'
gsettings set org.gnome.desktop.session idle-delay 0 # 0 is disabled.
