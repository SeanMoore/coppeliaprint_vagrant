#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Set number of virtual desktops.'
sudo apt-get install -y crudini
COMPIZ_PROFILE="$(crudini --get $HOME/.config/compiz-1/compizconfig/config general_ubuntu profile)"
dconf write "/org/compiz/profiles/$COMPIZ_PROFILE/plugins/core/hsize" 7
dconf write "/org/compiz/profiles/$COMPIZ_PROFILE/plugins/core/vsize" 7

echo 'Set key bindings for movement between workspaces.'
sudo apt-get install -y compizconfig-settings-manager
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up    "['<Control><Alt>Up']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down  "['<Control><Alt>Down']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left  "['<Control><Alt>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Control><Alt>Right']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up    "['<Shift><Control><Alt>Up']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down  "['<Shift><Control><Alt>Down']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-left  "['<Shift><Control><Alt>Left']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-right "['<Shift><Control><Alt>Right']"


RING=/org/compiz/profiles/unity/plugins/ring
dconf write $RING/next-all-key "'<Control><Alt>Tab'"
dconf write $RING/next-key "'<Alt>Tab'"
dconf write $RING/prev-all-key "'<Shift><Control><Alt>Tab'"
dconf write $RING/prev-key "'<Shift><Alt>Tab'"

# Need to append 'ring' to /org/compiz/profiles/unity-lowgfx/plugins/core/active-plugins
