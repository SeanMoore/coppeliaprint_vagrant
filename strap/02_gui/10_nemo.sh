#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

export DISPLAY=:0

echo 'Install and conifigure Nemo.'
sudo apt-get update
sudo apt-get install -y nemo
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search
gsettings set org.gnome.desktop.background show-desktop-icons false
gsettings set org.nemo.desktop show-desktop-icons true
# sudo apt-get remove -y nautilus # I'm not sure how to delete this without messing up the system.
