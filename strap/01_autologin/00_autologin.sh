#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Set up automatic log in.'
sudo apt-get install -y crudini
function set-lightdm() {
    sudo crudini --set /etc/lightdm/lightdm.conf SeatDefaults $@;
}
set-lightdm autologin-guest false
set-lightdm autologin-user vagrant
set-lightdm autologin-user-timeout 0
set-lightdm autologin-session lightdm-autologin
