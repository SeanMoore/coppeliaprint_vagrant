#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Upgrade apt-get packages."
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y