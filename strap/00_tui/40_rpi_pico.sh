#!/bin/bash
set -euo pipefail

echo "Configure RP2040 Code."
sudo apt-get install -y libudev-dev libusb-0.1-4 # The specific version is needed for OpenOCD.
rustup target install thumbv6m-none-eabi
cargo install probe-run
cargo install flip-link
pushd $HOME/CoppeliaPrint/CoppeliaPrint/CoppeliaPrintPico
    cargo build
popd