#!/bin/bash
set -euo pipefail

echo "Configure Raspberry Pi Code."
sudo apt-get install -y libudev-dev
rustup target install armv7-unknown-linux-gnueabihf
