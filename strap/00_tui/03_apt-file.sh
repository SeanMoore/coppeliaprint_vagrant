#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Install apt-file."
sudo apt-get install -y apt-file
sudo apt-file update
