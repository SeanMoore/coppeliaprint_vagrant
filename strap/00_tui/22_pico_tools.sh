#!/bin/bash
set -euo pipefail

echo "Installing Raspberry Pi Pico Tools."

JNUM=$(nproc)

OUTDIR="$HOME/pico"
mkdir -p $OUTDIR
pushd $OUTDIR
    for REPO in picoprobe picotool
    do
        DEST="$OUTDIR/$REPO"
        
        if [ -d $DEST ]
        then
            echo "$DEST already exists so skipping"
        else
            REPO_URL="https://github.com/raspberrypi/${REPO}.git"
            echo "Cloning $REPO_URL"
            git clone $REPO_URL
        fi

        pushd $DEST
            mkdir -p build
            pushd build
                cmake $DEST
                make -j$JNUM

                if [[ "$REPO" == "picotool" ]]
                then
                    echo "Installing picotool to /usr/local/bin/picotool"
                    sudo cp picotool /usr/local/bin/
                fi
            popd
        popd
    done
popd