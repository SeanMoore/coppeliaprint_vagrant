#!/bin/bash
set -euo pipefail

echo "Install rustlang"
pushd /tmp
    wget https://sh.rustup.rs/rustup-init.sh
    chmod +x rustup-init.sh
    ./rustup-init.sh -y
popd