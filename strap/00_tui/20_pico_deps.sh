#!/bin/bash
set -euo pipefail

echo "Installing Raspberry Pi Pico Dependencies"
SDK_DEPS="cmake gcc-arm-none-eabi gcc g++"
TOOLS_DEPS="pkg-config"
OPENOCD_DEPS="gdb-multiarch automake autoconf build-essential texinfo libtool libftdi-dev libusb-1.0-0-dev"
sudo apt install -y $SDK_DEPS $TOOLS_DEPS $OPENOCD_DEPS
