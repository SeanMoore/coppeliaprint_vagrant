#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Set up host SSH keys.'
cat >>$HOME/.myrc <<EOF
export SSH_PATH=/home/vagrant/.ssh_host
EOF