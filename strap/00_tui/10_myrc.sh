#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Set up bash initialization.'
touch $HOME/.myrc
echo "source ~/.myrc" >> $HOME/.bashrc
echo "source ~/.myrc" >> $HOME/.profile