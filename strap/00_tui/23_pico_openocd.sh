#!/bin/bash
set -euo pipefail

echo "Installing Raspberry Pi Pico OpenOCD."

JNUM=$(nproc)

OUTDIR="$HOME/pico"
mkdir -p $OUTDIR
pushd $OUTDIR
    REPO=openocd
    DEST="$OUTDIR/$REPO"

    # Should we include picoprobe support (which is a Pico acting as a debugger for another Pico)
    INCLUDE_PICOPROBE=0
    OPENOCD_BRANCH="rp2040"
    OPENOCD_CONFIGURE_ARGS="--enable-ftdi --enable-sysfsgpio --enable-bcm2835gpio"
    if [[ "$INCLUDE_PICOPROBE" == 1 ]]
    then
        OPENOCD_BRANCH="picoprobe"
        OPENOCD_CONFIGURE_ARGS="$OPENOCD_CONFIGURE_ARGS --enable-picoprobe"
    fi

    if [ -d $DEST ]
    then
        echo "$DEST already exists so skipping"
    else
        REPO_URL="https://github.com/raspberrypi/${REPO}.git"
        echo "Cloning $REPO_URL"
        git clone $REPO_URL -b $OPENOCD_BRANCH --depth=1
    fi

    pushd openocd
        ./bootstrap
        ./configure $OPENOCD_CONFIGURE_ARGS
        make -j$JNUM
        sudo make install
    popd
popd