#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Install Ubuntu desktop."
sudo bash -c "echo \"/usr/sbin/lightdm\" > /etc/X11/default-display-manager"
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ubuntu-unity-desktop \
    dbus-x11 \
;
