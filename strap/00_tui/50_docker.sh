#!/bin/bash
set -euo pipefail

echo "Install docker."
DOCKER_ORIGIN=http://download.docker.com # https fails in proxy.
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
;
sudo mkdir -p /etc/apt/keyrings
curl -fsSL ${DOCKER_ORIGIN}/linux/ubuntu/gpg | sudo gpg --dearmor --yes -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] ${DOCKER_ORIGIN}/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io \
    docker-compose-plugin \
;

echo "Configure docker."
sudo groupadd -f docker
sudo usermod -aG docker $USER
