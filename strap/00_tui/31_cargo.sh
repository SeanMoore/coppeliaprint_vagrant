#!/bin/bash
set -euo pipefail

echo "Configure cargo."
sudo apt-get install -y pkg-config libssl-dev
cargo install cargo-edit --features vendored-openssl

mkdir -p $HOME/.cargo
cat >>$HOME/.cargo/config.toml <<EOF
[build]
target-dir = "$HOME/rust/Project"
EOF