#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Configure apt-get for apt-cacher-ng."
echo 'Acquire::http::Proxy "http://192.168.63.99:3142";' | sudo tee /etc/apt/apt.conf.d/00proxy
echo 'Acquire::HTTP::Proxy::personal.repository.local "DIRECT";' | sudo tee -a /etc/apt/apt.conf.d/00proxy

echo "Poll until the apt-cacher-ng server is up."
while ! nc -z 192.168.63.99 3142
do
    sleep 1
done