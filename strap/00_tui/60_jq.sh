#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Install JSON Query, jq."
sudo apt-get install -y jq