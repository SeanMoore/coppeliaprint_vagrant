#!/bin/bash
set -euo pipefail

echo "Installing Raspberry Pi Pico SDK."

OUTDIR="$HOME/pico"
mkdir -p $OUTDIR
pushd $OUTDIR
    for REPO in sdk examples extras playground
    do
        DEST="$OUTDIR/pico-$REPO"

        if [ -d $DEST ]
        then
            echo "$DEST already exists so skipping"
        else
            REPO_URL="https://github.com/raspberrypi/pico-${REPO}.git"
            echo "Cloning $REPO_URL"
            git clone -b master $REPO_URL
        fi

        pushd $DEST
            git submodule update --init
        popd

        # Define PICO_SDK_PATH in ~/.myrc
        VARNAME="PICO_${REPO^^}_PATH"
        echo "Adding $VARNAME to ~/.myrc"
        echo "export $VARNAME=$DEST" >> ~/.myrc
    done
popd