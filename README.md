# CoppeliaMedia_vagrant

## Requirements

- [ton_vagrant](https://github.com/219-design/ton_vagrant)
- [ton_vagrant_cache](https://github.com/219-design/ton_vagrant_cache)
- [qt-unified-linux-x64-4.1.0-online.run](https://www.qt.io/download-qt-installer) in the root directory of the repository.

## Initialization

Check ton_vagrant for common initial setup instructions.

Create a file at `CoppeliaMedia_vagrant/user/qt_credentials.sh` with your Qt credentials.

```bash
function credentialized() {
    $@ \
        --email 'you@example.com' \
        --password 'Y0urP@55\/\/0rd' \
	;
}
```

Build or start up the machine.

```bash
vagrant up cache CoppeliaMedia_vagrant
```

